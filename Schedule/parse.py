from openpyxl import load_workbook
import json
import dictfier

class Many(object):
    def __init__(self, data):
        self.data = data

class Lesson:
    def __init__(self, slot, name, instructor, room, general):
        self.courseslot = slot
        self.coursename = name
        self.instructor = instructor
        self.roomnumber = room
        self.forall = general

class Day:
    def __init__(self, day, lessonlist):
        self.day = day
        self.lessons = Many(lessonlist)
    
class Group:
    def __init__(self, grpname, weekdaylessonlist):
        self.groupname = grpname
        self.weeklessons = Many(weekdaylessonlist)

class Course:
    def __init__(self, coursename, grouplist):
        self.coursename = coursename
        self.groups = Many(grouplist)

day = ("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")
workbook = load_workbook(filename="Core Courses_Spring_2019-2020.xlsx", data_only=True)
sheet = workbook["BS,MS_Spring 2019"]
university = list()

def getdayinfo(jstart, irow, iday):
    lessons = list()
    islot = 1
    while(islot < 8):
        for lesson in sheet.iter_cols(   min_col=jstart, max_col=jstart,
                                        min_row=irow, max_row= irow + 2, values_only=True):
            if (all(lesson)):
                status = (lesson[0][-5:] == "(Lec)"  or lesson[0][-5:] == "rial)" or lesson[0][-5:] == "tut.)")
                mylesson = Lesson(islot, lesson[0], lesson[1], int(lesson[2]), status )
                lessons.append(mylesson)
            islot += 1
        irow += 3 
    return Day(day[iday], lessons)

def getcourseinfo(jstart, numCol, step):
    grplist = list()
    jbsyear = jstart
    while (jbsyear < jstart + numCol):

        weeklylessons = list()
        irow = 4
        for iday in range(0, 7):
            weeklylessons.append(getdayinfo(jbsyear, irow, iday))
            irow += 22
        
        grpname = sheet.cell(row=2, column=jbsyear).value
        grplist.append(Group(grpname, weeklylessons))
        jbsyear += step
    coursename = sheet.cell(row=1, column=jstart).value
    return Course(coursename, grplist)


##########
#BSYear1
##########
university.append(getcourseinfo(2, 12, 2))

##########
#BSYear2
##########
university.append(getcourseinfo(14, 6, 1))


##########
#BSYear3
##########
university.append(getcourseinfo(20, 6, 1))

##########
#BSYear4
##########
university.append(getcourseinfo(26, 4, 1))

##########
#MSYear1
##########
university.append(getcourseinfo(30, 5, 1))

##########
#MSYear2
##########
university.append(getcourseinfo(35, 1, 1))


query = [[
    "coursename",
    {
        "groups": [[
            "groupname",
            {
                "weeklessons": [[
                    "day",
                    {
                        "lessons" : [[
                            "courseslot",
                            "coursename",
                            "instructor",
                            "roomnumber",
                            "forall"
                        ]]
                    }
                ]]
            }
        ]]
    }
]]

result = dictfier.dictfy(
    university,
    query,
    flat_obj=lambda obj, parent: obj,
    nested_iter_obj=lambda obj, parent: obj.data,
    nested_flat_obj=lambda obj, parent: obj.data
)

#print(json.dumps(result))

f = open("sample.json", "w")
f.write(str(json.dumps(result)))
f.close()

    


