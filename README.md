The Frontend subproject contains both the main api of our application (i.e the attendance tracking) and the webserver which serves webpages to the frontend. This is a full stack application (MERN) 
as we used node.js, mongoDB, expressjs and Reactjs.
The server is deployed on Heroku, you can find the app here:
https://attendance-tracking-iu.herokuapp.com/
To run the server locally, you can do the following:

clone the git repository to your local machine.
set up the node development envirnment: https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/development_environment

run the command npm install to install the dependencies of the package.json file
run the command npm start

open the browser and go to the local url: http://localhost:8080/
